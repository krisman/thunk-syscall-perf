

CFLAGS=-O0 -g3

all: signal olga

olga:
	 rsync -r * olga:

signal:
	gcc -c $(CFLAGS) syscall.S -o syscall.o
	gcc -c $(CFLAGS) thunk_syscall_perf.c -o thunk_syscall_perf.o
	gcc $(CFLAGS) syscall.o thunk_syscall_perf.o -o thunk_syscall_perf


clean:
	rm thunk_syscall_perf *.o
