#define _GNU_SOURCE

#include <stdio.h>
#include <sys/prctl.h>
#include <sys/sysinfo.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/syscall.h>


#include <ucontext.h>

#define TEST_COUNT 4000000

extern unsigned int raw_syscall(long number,
				unsigned long arg1, unsigned long arg2,
				unsigned long arg3, unsigned long arg4,
				unsigned long arg5, unsigned long arg6);

extern void* (syscall_instruction)(void);
extern void* (syscall_instruction_entry)(void);
extern void* (syscall_thunk)(void);

static int trapped_call_count, native_call_count;

#define EMPTY_SYSCALL 440

static void handle_sigsys(int sig, siginfo_t *info, void *ucontext)
{
	struct ucontext_t *ctx = ucontext;
	mcontext_t *mctx = &ctx->uc_mcontext;
	int r;

	if (info->si_syscall == EMPTY_SYSCALL) {
		mctx->gregs[REG_RAX] = 0;
		goto out;
	}

	if (info->si_syscall > 0xf000) {
		trapped_call_count ++;
		mctx->gregs[REG_RAX] = 0xdeadbeef;
		goto out;
	}

	native_call_count++;
	r = raw_syscall(mctx->gregs[REG_RAX], mctx->gregs[REG_RDI],
			mctx->gregs[REG_RSI], mctx->gregs[REG_RDX],
			mctx->gregs[REG_R10], mctx->gregs[REG_R8],
			mctx->gregs[REG_R9]);
	mctx->gregs[REG_RAX] = r;

out:
	__asm__ volatile("movq $0xf, %rax");
	__asm__ volatile("leaveq");
	__asm__ volatile("add $0x8, %rsp");
	__asm__ volatile("jmp syscall_instruction_entry");
}

unsigned int do_thunk(int num, unsigned long arg1, unsigned long arg2,
		      unsigned long arg3, unsigned long arg4, unsigned long arg5,
		      unsigned long arg6)
{
	if (num == EMPTY_SYSCALL)
		return 0;

	if (num > 0xf000) {
		trapped_call_count++;
		return 0xdeadbeef;
	}

	return raw_syscall(num, arg1, arg2, arg3, arg4,arg5,arg6);
}

#define PRCTL_REDIRECTION_SIGSYS 1

static double perf_syscall()
{
	struct sysinfo info;
	struct timespec ts;
	unsigned int i;
	double t1, t2;
	int addr;

	addr = 0;

	clock_gettime(CLOCK_MONOTONIC, &ts);
	t1 = ts.tv_sec + 1.0e-9 * ts.tv_nsec;
	for (i = 0; i < TEST_COUNT; ++i) {
		if (syscall(EMPTY_SYSCALL)) {
			perror("sysinfo");
			exit(-1);
		}
	}
	clock_gettime(CLOCK_MONOTONIC, &ts);
	t2 = ts.tv_sec + 1.0e-9 * ts.tv_nsec;
	return (t2 - t1) / TEST_COUNT;
}

int main(int argc, char *argv[])
{
	double time1, time2;
	int ret;
	void *entry_point = NULL;
	const struct sigaction act = {
		.sa_sigaction = handle_sigsys,
		.sa_flags = SA_SIGINFO,
		.sa_mask = 0
	};
	int direct = 0;

	if (argc != 2) {
		printf("wrong command line.\n");
		return -1;
	}

	if (strcmp(argv[1], "signal") == 0)
		direct = 0;
	else if (strcmp(argv[1], "thunk") == 0)
		direct = 1;
	else {
		printf("unknown command line.\n");
		return -1;
	}

	time1 = perf_syscall();
	printf("Avg sycall time %.0lfns.\n", time1 * 1.0e9);

	trapped_call_count = native_call_count = 0;

	if (prctl(PR_SET_SPECULATION_CTRL, PR_SPEC_STORE_BYPASS, PR_SPEC_ENABLE,0, 0)) {
		printf("Error enabling speculation: %s.\n", strerror(errno));
		return -1;
	}
	ret = prctl(PR_GET_SPECULATION_CTRL, PR_SPEC_STORE_BYPASS, 0, 0, 0);
	if (ret < 0) {
		printf("Error getting speculation: %s.\n", strerror(errno));
		return -1;
	}

	if (!direct) {
		ret = sigaction(SIGSYS, &act, NULL);
		if (ret) {
			printf("Error sigaction: %s.\n", strerror(errno));
			return -1;
		}
		entry_point = NULL;
	} else {
		entry_point = syscall_thunk;
	}

	if (prctl(59, entry_point, syscall_instruction,
		  direct? 0 : PRCTL_REDIRECTION_SIGSYS , 0, 0)) {
		printf("Error setting syscall thunk: %s.\n", strerror(errno));
		return -1;
	}

	__asm__ volatile("movq $0xf001,%rax");
	__asm__ volatile("syscall");
	__asm__ volatile("movl %%eax, %0" : "=m"(ret));

	printf("ret %#x.\n", ret);
	if (!trapped_call_count)
	{
		printf("syscall trapping does not work.\n");
		exit(-1);
	}

	time2 = perf_syscall();
	printf("trapped_call_count %u, native_call_count %u.\n", trapped_call_count, native_call_count);

	printf("Avg sycall time %.0lfns.\n", time2 * 1.0e9);
	printf("Interception overhead: %.1lf%% (+%.0lfns).\n", 100.0 * (time2 / time1 - 1.0) , 1.0e9 * (time2 - time1));
}
